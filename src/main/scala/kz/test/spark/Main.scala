package kz.test.spark

import org.apache.spark.sql.SparkSession

object Main {

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().appName("Spark streaming").getOrCreate()
    import spark.implicits._
    val stream = spark.readStream.format("socket").option("host", "localhost").option("port", 9999).load()
    val words = stream.as[String].flatMap(_.split(" "))
    val counts = words.groupBy("value").count()
    val query = counts.writeStream.outputMode("complete").format("console").start()
    query.awaitTermination()
  }
}
